#!/bin/bash

# Script to build with MacPorts-provided Qt4.  Doesn't include install.
# Derived from install.sh, with extra arg to find Qt4.
# This is a temporary hack.  Note that MacPorts prefix is hardcoded.

if [ ! -d build  ]
then
    mkdir build
fi

cd build
# Note that we can pass additional arguments to cmake via this script's
# command line
cmake -DCMAKE_BUILD_TYPE=Release \
      -DQT_QMAKE_EXECUTABLE=/opt/local/libexec/qt4/bin/qmake \
      "$@" ../
make
